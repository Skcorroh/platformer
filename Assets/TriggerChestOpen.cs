﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChestOpen : MonoBehaviour
{

	public Animator anim;
	public GameObject coinPref;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.gameObject.tag == "Player") {
			anim.SetTrigger ("Touched");

			UnityEngine.Object.Instantiate (coinPref);

			Vector3 coinPos = new Vector3 (transform.position.x, transform.position.y, transform.position.z - 0.1f);
			Debug.Log (transform.position.z - 0.1f);
//			GameObject newCoin = (GameObject)
			Instantiate (coinPref, coinPos, transform.rotation);
		}
	}
}
